use std::string::String;
use std::vec::Vec;

pub trait BufferZerro {
    fn zerro_buff(&mut self);
}

impl BufferZerro for String {
    fn zerro_buff(&mut self) {
        unsafe {
            self.as_mut_vec().into_iter().for_each(|x| *x = 0);
        }
    }
}

impl<T: Default> BufferZerro for Vec<T> {
    fn zerro_buff(&mut self) {
        self.into_iter().for_each(|x| *x = T::default());
    }
}
