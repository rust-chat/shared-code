extern crate serde;
extern crate serde_json;
extern crate sha2;

use error::Result;
use sha2::Digest;

pub mod cleared_buffer;
pub mod error;
pub mod net;

pub trait ToBytes {
    fn to_bytes(&self) -> Result<Vec<u8>>;
}

pub trait FromBytes {
    fn from_bytes(buff: Vec<u8>) -> Result<Box<Self>>;
}

pub fn sha512<T: ToString>(text: T) -> Result<Vec<u8>> {
    let text = text.to_string();
    let mut hasher = sha2::Sha512::new();
    hasher.update(text.as_bytes());

    let result = hasher.finalize().to_vec();

    Ok(result)
}
