use serde::{Deserialize, Serialize};
use std::{
    io::{Read, Write},
    net::{TcpStream, ToSocketAddrs, SocketAddr},
};

use crate::{
    error::{Error, Result},
    FromBytes, ToBytes,
};

const SIZE_BUFFER_SIZE: usize = usize::BITS as usize / 8;

pub struct TcpConnection(TcpStream);

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Auth {
    username: String,
    pwd_hash: Vec<u8>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Request {
    action: RequestAction,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Response {
    status: ResponseStatus,
    content: ResponseData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum RequestAction {
    RegisterUser(Auth),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum ResponseStatus {
    Ok,
    Failed,
    AuthFailed,
    RegisterFailed,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum ResponseData {
    None,
}

impl ToBytes for Request {
    fn to_bytes(&self) -> Result<Vec<u8>> {
        match serde_json::to_string(self) {
            Ok(json) => Ok(json.as_bytes().to_vec()),
            Err(err) => Err(Error::JsonError(err)),
        }
    }
}

impl FromBytes for Request {
    fn from_bytes(buff: Vec<u8>) -> Result<Box<Self>> {
        let json_string = match String::from_utf8(buff) {
            Ok(json) => json,
            Err(err) => return Err(Error::FromUTF8Error(err)),
        };

        match serde_json::from_str(&json_string) {
            Ok(json) => Ok(Box::new(json)),
            Err(err) => Err(Error::JsonError(err)),
        }
    }
}

impl Request {
    pub fn new(action: RequestAction) -> Self {
        Self { action }
    }

    pub fn send_request<T: ToSocketAddrs>(&self, addr: T) -> Result<Response> {
        let mut conn = TcpConnection::new(addr).unwrap();

        conn.send_data(self.clone())?;
        conn.recive_data()
    }

    pub fn recive_request(conn: &mut TcpConnection) -> Result<Self> {
        conn.recive_data()
    }

    pub fn action(&self) -> &RequestAction {
        &self.action
    }
}

impl Auth {
    pub fn new<T: ToString, S: ToString>(username: T, pwd: S) -> Result<Self> {
        let hash = crate::sha512(pwd)?;

        Ok(Self {
            username: username.to_string(),
            pwd_hash: hash,
        })
    }

    pub fn username(&self) -> &str {
        self.username.as_ref()
    }

    pub fn pwd_hash(&self) -> &[u8] {
        self.pwd_hash.as_ref()
    }
}

impl TcpConnection {
    pub fn new<T: ToSocketAddrs>(addr: T) -> Result<Self> {
        let stream = match TcpStream::connect(addr) {
            Ok(con) => con,
            Err(err) => return Err(Error::IoError(err)),
        };

        Ok(Self(stream))
    }

    pub fn from_stream(stream: TcpStream) -> Self {
        Self(stream)
    }

    pub fn send_data<T: ToBytes>(&mut self, data: T) -> Result<usize> {
        let mut payload = data.to_bytes()?;
        let mut size_buffer = payload.len().to_be_bytes();

        match self.0.write_all(&mut size_buffer) {
            Err(err) => return Err(Error::IoError(err)),
            _ => {}
        }

        match self.0.write_all(&mut payload) {
            Err(err) => Err(Error::IoError(err)),
            _ => Ok(payload.len()),
        }
    }

    pub fn recive_data<F: FromBytes>(&mut self) -> Result<F> {
        let mut size_buffer: [u8; SIZE_BUFFER_SIZE] = [0; SIZE_BUFFER_SIZE];
        match self.0.read_exact(&mut size_buffer) {
            Err(err) => return Err(Error::IoError(err)),
            _ => {}
        }
        let payload_size = usize::from_be_bytes(size_buffer);
        let mut payload = vec![0u8; payload_size];

        match self.0.read_exact(&mut payload) {
            Err(err) => return Err(Error::IoError(err)),
            _ => {}
        }

        Ok(*F::from_bytes(payload)?)
    }

    pub fn peer_addr(&self) -> Result<SocketAddr> {
        match self.0.peer_addr() {
            Ok(addr) => Ok(addr),
            Err(err) => Err(Error::IoError(err)),
        }
    }
}

impl Response {
    pub fn new() -> Self {
        Self {
            status: ResponseStatus::Failed,
            content: ResponseData::None,
        }
    }

    pub fn send(&self, _conn: &mut TcpConnection) -> Result<usize> {
        _conn.send_data(self.clone())
    }

    pub fn set_status(&mut self, status: ResponseStatus) -> &mut Self {
        self.status = status;
        self
    }

    pub fn set_content(&mut self, data: ResponseData) -> &mut Self {
        self.content = data;
        self
    }
}

impl ToBytes for Response {
    fn to_bytes(&self) -> Result<Vec<u8>> {
        match serde_json::to_string(self) {
            Ok(json) => Ok(json.as_bytes().to_vec()),
            Err(err) => Err(Error::JsonError(err)),
        }
    }
}

impl FromBytes for Response {
    fn from_bytes(buff: Vec<u8>) -> Result<Box<Self>> {
        let json_str = match String::from_utf8(buff) {
            Ok(json) => json,
            Err(err) => return Err(Error::FromUTF8Error(err)),
        };

        match serde_json::from_str(&json_str) {
            Ok(json) => Ok(json),
            Err(err) => Err(Error::JsonError(err))
        }
    }
}
