use std::result;

pub type Result<T> = result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    IoError(std::io::Error),
    VecToArrayFailed(Vec<u8>),
    JsonError(serde_json::Error),
    FromUTF8Error(std::string::FromUtf8Error),
    RegistrationFailed(String),
    ConfigInitialized,
    RusqliteError(rusqlite::Error),
    R2D2Error(r2d2::Error),
}
